FROM node:18.17.0-alpine 

#COPY nginx /etc/nginx/
WORKDIR /src

COPY package.json .
RUN npm install --force

COPY . .

RUN npm run build

EXPOSE 9070

CMD ["npm","start"]