import withPWA from 'next-pwa'

export default withPWA({
  dest: 'public',
  disable: process.env.NODE_ENV === 'development',
  register: true,
  scope: '/push-notification',
  sw: 'service-worker.js',
  //...
}) 

// const withPWA = require("@ducanh2912/next-pwa").default({
//   dest: 'public',
//   disable: process.env.NODE_ENV === 'development',
//   register: true,
//   scope: '/push-notification',
//   sw: 'service-worker.js',
//   //...
// });

